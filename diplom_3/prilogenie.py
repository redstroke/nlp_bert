import torch
from transformers import BertTokenizer, BertForSequenceClassification
from tkinter import *

def classif(str):
    # inputs = tokenizer1(str, return_tensors="pt", max_length = 64, pad_to_max_length = True)
    inputs = tokenizer1(str, return_tensors="pt")
    labels = torch.tensor([1]).unsqueeze(0)  # Batch size 1
    outputs = model1(**inputs, labels=labels)
    if outputs[0] < 0.01:
        lbl1.configure(text = "Фраза грамматически корректна", fg = "green")
    else: lbl1.configure(text = "Фраза грамматически некорректна" , fg = "#88228a")

def clicked():
    classif(txt.get(0.0, END))

fialka = '#9d89c9'
model1 = BertForSequenceClassification.from_pretrained('/diplom_3')
tokenizer1 = BertTokenizer.from_pretrained('/diplom_3/vocab.txt')
model1.eval()

window = Tk()
window.title("Грамматическая корректность предложений")
lbl = Label(window, text = "Введите фразу:", font =("Arial", 20), bg = fialka)
lbl.pack(pady = 40)
window.geometry('700x400')
textt = StringVar()
# txt = Entry(window, width = 50, font =("Arial", 14), bg = '#ae9fcf', textvariable = textt)
txt = Text(window, width = 50, font =("Arial", 14), bg = '#ae9fcf',  height = 3)
txt.pack(pady = 10)
btn = Button(window, text = "Проверить", command = clicked, font =("Arial", 20), bg = '#8472ab')
btn.pack(pady = 10)
lbl1 = Label(window, text = " ", font =("Arial Bold", 20), bg = fialka)
lbl1.pack(pady = 10)
# lbl1.grid(column = 0, row = 4)

window['bg'] = fialka
window.mainloop()